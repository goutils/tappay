package tappay

// Amount json
type Amount struct {
	UpperLimit int `json:"upper_limit,omitempty"`
	LowerLimit int `json:"lower_limit,omitempty"`
}

// BankTransactionTime json
type BankTransactionTime struct {
	StartTimeMillis int `json:"start_time_millis,omitempty"`
	EndTimeMillis   int `json:"end_time_millis,omitempty"`
}

// BankTransactionTime1 json
type BankTransactionTime1 struct {
	StartTimeMillis string `json:"start_time_millis,omitempty"`
	EndTimeMillis   string `json:"end_time_millis,omitempty"`
}

// BindCardReq json
type BindCardReq struct {
	Prime                     string           `json:"prime"`
	PartnerKey                string           `json:"partner_key"`
	MerchantID                string           `json:"merchant_id"`
	MerchantGroupID           string           `json:"merchant_group_id,omitempty"`
	Currency                  string           `json:"currency,omitempty"`
	ThreeDomainSecure         bool             `json:"three_domain_secure,omitempty"`
	ResultURL                 ResultURL        `json:"result_url,omitempty"`
	Cardholder                Cardholder       `json:"cardholder,omitempty"`
	CardholderVerify          CardholderVerify `json:"cardholder_verify,omitempty"`
	KycVerificationMerchantId string           `json:"kyc_verification_merchant_id,omitempty"`
}

// BindCardResp json
type BindCardResp struct {
	Status              int         `json:"status,omitempty"`
	Msg                 string      `json:"msg,omitempty"`
	RecTradeID          string      `json:"rec_trade_id,omitempty"`
	BankTransactionID   string      `json:"bank_transaction_id,omitempty"`
	Currency            string      `json:"currency"`
	AuthCode            string      `json:"auth_code,omitempty"`
	MerchantID          string      `json:"merchant_id,omitempty"`
	Acquirer            string      `json:"acquirer,omitempty"`
	CardSecret          CardSecret  `json:"card_secret,omitempty"`
	CardInfo            CardInfo    `json:"card_info"`
	Millis              int         `json:"millis,omitempty"`
	BankTransactionTime interface{} `json:"bank_transaction_time,omitempty"`
	BankResultCode      string      `json:"bank_result_code,omitempty"`
	BankResultMsg       string      `json:"bank_result_msg,omitempty"`
	CardIdentifier      string      `json:"card_identifier,omitempty"`
	PaymentURL          string      `json:"payment_url,omitempty"`
}

// Card json
type Card struct {
	CardToken      string `json:"card_token"`
	BinCode        string `json:"bin_code"`
	LastFour       string `json:"last_four"`
	Issuer         string `json:"issuer"`
	Funding        int    `json:"funding"`
	Type           int    `json:"type"`
	Level          string `json:"level"`
	Country        string `json:"country"`
	CountryCode    string `json:"country_code"`
	CardIdentifier string `json:"card_identifier"`
}

// CardArtInfo json
type CardArtInfo struct {
	CardArtStatus    string `json:"card_art_status"`
	IsRealCardFace   bool   `json:"is_real_card_face"`
	Image            Image  `json:"image"`
	ForegroundColor  string `json:"foreground_color"`
	MaskedCardNumber string `json:"masked_card_number"`
	Issuer           string `json:"issuer"`
}

// CapCancelReq json
type CapCancelReq struct {
	PartnerKey string `json:"partner_key"`
	RecTradeID string `json:"rec_trade_id"`
}

// CapCancelResp json
type CapCancelResp struct {
	Status     int    `json:"status"`
	Msg        string `json:"msg"`
	Currency   string `json:"currency"`
	RecTradeID string `json:"rec_trade_id"`
}

// CardMetadataReq json
type CardMetadataReq struct {
	PartnerKey string `json:"partner_key"`
	CardKey    string `json:"card_key"`
	CardToken  string `json:"card_token"`
}

// CardMetadataResp json
type CardMetadataResp struct {
	Status   int      `json:"status"`
	Msg      string   `json:"msg"`
	CardInfo CardInfo `json:"card_info"`
}

// CapTodayReq json
type CapTodayReq struct {
	PartnerKey string `json:"partner_key"`
	RecTradeID string `json:"rec_trade_id"`
}

// CapTodayResp json
type CapTodayResp struct {
	Status    int    `json:"status"`
	Msg       string `json:"msg"`
	CapMillis int    `json:"cap_millis"`
	Currency  string `json:"currency"`
}

// Cardholder json
type Cardholder struct {
	PhoneNumber string `json:"phone_number"`
	Name        string `json:"name"`
	Email       string `json:"email"`
	ZipCode     string `json:"zip_code,omitempty"`
	Address     string `json:"address,omitempty"`
	NationalID  string `json:"national_id,omitempty"`
	MemberID    string `json:"member_id,omitempty"`
}

// CardholderVerify json
type CardholderVerify struct {
	PhoneNumber bool `json:"phone_number,omitempty"`
	NationalID  bool `json:"national_id,omitempty"`
}

// CardInfo json
type CardInfo struct {
	BinCode     string `json:"bin_code,omitempty"`
	LastFour    string `json:"last_four,omitempty"`
	Issuer      string `json:"issuer,omitempty"`
	IssuerZhTw  string `json:"issuer_zh_tw,omitempty"`
	BankID      string `json:"bank_id,omitempty"`
	Funding     int    `json:"funding,omitempty"`
	Type        int    `json:"type,omitempty"`
	Level       string `json:"level,omitempty"`
	Country     string `json:"country,omitempty"`
	CountryCode string `json:"country_code,omitempty"`
	ExpiryDate  string `json:"expiry_date,omitempty"`
	TokenStatus string `json:"token_status,omitempty"`
}

// CardSecret json
type CardSecret struct {
	CardToken string `json:"card_token,omitempty"`
	CardKey   string `json:"card_key,omitempty"`
}

// ExtraInfo json
type ExtraInfo struct {
	InstallOrderNo    string `json:"install_order_no,omitempty"`
	InstallPeriod     string `json:"install_period,omitempty"`
	InstallPayFee     string `json:"install_pay_fee,omitempty"`
	InstallPay        string `json:"install_pay,omitempty"`
	InstallDownPay    string `json:"install_down_pay,omitempty"`
	InstallDownPayFee string `json:"install_down_pay_fee,omitempty"`

	Installment     string `json:"installment,omitempty"`
	InstallmentType string `json:"installment_type,omitempty"`
	FirstAmt        string `json:"first_amt,omitempty"`
	EachAmt         string `json:"each_amt,omitempty"`
	Fee             string `json:"fee,omitempty"`

	Inst      int `json:"inst,omitempty"`
	InstFirst int `json:"inst_first,omitempty"`
	InstEach  int `json:"inst_each,omitempty"`

	PeriodNumber string `json:"period_number,omitempty"`

	ITA  string `json:"ITA,omitempty"`
	IP   string `json:"IP,omitempty"`
	IPA  string `json:"IPA,omitempty"`
	IFPA string `json:"IFPA,omitempty"`
}

// Filter json
type Filter struct {
	Time              interface{} `json:"time,omitempty"`
	Amount            interface{} `json:"amount,omitempty"`
	Cardholder        interface{} `json:"cardholder,omitempty"`
	MerchantID        []string    `json:"merchant_id,omitempty"`
	RecordStatus      int         `json:"record_status,omitempty"`
	RecTradeID        string      `json:"rec_trade_id"`
	OrderNumber       string      `json:"order_number,omitempty"`
	BankTransactionID string      `json:"bank_transaction_id,omitempty"`
	AuthCode          string      `json:"auth_code,omitempty"`
	Currency          string      `json:"currency,omitempty"`
	Tsp               bool        `json:"tsp,omitempty"`
	CardIdentifier    string      `json:"card_identifier,omitempty"`
}

// GetBarcodeReq json
type GetBarcodeReq struct {
	PartnerKey        string `json:"partner_key"`
	CardKey           string `json:"card_key"`
	CardToken         string `json:"card_token"`
	BarcodeLength     int    `json:"barcode_length"`
	BarcodeUpdateSecs int    `json:"barcode_update_secs"`
}

// GetBarcodeResp json
type GetBarcodeResp struct {
	Status            int    `json:"status"`
	Msg               string `json:"msg"`
	Barcode           string `json:"barcode"`
	BarcodeUpdateSecs int    `json:"barcode_update_secs"`
}

// GetMemberCardReq json
type GetMemberCardReq struct {
	PartnerKey string `json:"partner_key"`
	MemberID   string `json:"member_id"`
}

// GetMemberCardResp json
type GetMemberCardResp struct {
	Status   int    `json:"status"`
	Msg      string `json:"msg"`
	MemberID string `json:"member_id"`
	Cards    []Card `json:"cards"`
}

// Image json
type Image struct {
	URL    string `json:"url"`
	Width  int    `json:"width"`
	Height int    `json:"height"`
}

// InstalmentInfo json
type InstalmentInfo struct {
	NumberOfInstalments string `json:"number_of_instalments,omitempty"`
	FirstPayment        string `json:"first_payment,omitempty"`
	EachPayment         string `json:"each_payment,omitempty"`
	ExtraInfo           ExtraInfo
}

// MerchantReferenceInfo json
type MerchantReferenceInfo struct {
	AffiliateCodes []string `json:"affiliate_codes,omitempty"`
}

// OrderBy json
type OrderBy struct {
	Attribute    string `json:"attribute,omitempty"`
	IsDescending bool   `json:"is_descending,omitempty"`
}

// PayByBarcodeReq json
type PayByBarcodeReq struct {
	Barcode            string `json:"barcode"`
	PartnerKey         string `json:"partner_key"`
	MerchantID         string `json:"merchant_id"`
	MerchantGroupID    string `json:"merchant_group_id,omitempty"`
	Amount             int    `json:"amount"`
	Currency           string `json:"currency,omitempty"`
	OrderNumber        string `json:"order_number,omitempty"`
	BankTransactionID  string `json:"bank_transaction_id,omitempty"`
	Details            string `json:"details"`
	DelayCaptureInDays int    `json:"delay_capture_in_days,omitempty"`
	RetryMode          string `json:"retry_mode,omitempty"`
	Redeem             bool   `json:"redeem,omitempty"`
	AdditionalData     string `json:"additional_data,omitempty"`
}

// PayByBarcodeResp json
type PayByBarcodeResp struct {
	Status                int                   `json:"status"`
	Msg                   string                `json:"msg"`
	RecTradeID            string                `json:"rec_trade_id"`
	BankTransactionID     string                `json:"bank_transaction_id"`
	Amount                int                   `json:"amount"`
	Currency              string                `json:"currency"`
	AuthCode              string                `json:"auth_code"`
	CardInfo              CardInfo              `json:"card_info"`
	OrderNumber           string                `json:"order_number"`
	Acquirer              string                `json:"acquirer"`
	TransactionTimeMillis int                   `json:"transaction_time_millis"`
	BankTransactionTime   BankTransactionTime   `json:"bank_transaction_time"`
	BankResultCode        string                `json:"bank_result_code"`
	BankResultMsg         string                `json:"bank_result_msg"`
	RedeemInfo            RedeemInfo            `json:"redeem_info"`
	CardIdentifier        string                `json:"card_identifier"`
	MerchantReferenceInfo MerchantReferenceInfo `json:"merchant_reference_info"`
}

// PayByPrimeReq json
type PayByPrimeReq struct {
	Prime                  string           `json:"prime"`
	PartnerKey             string           `json:"partner_key"`
	MerchantID             string           `json:"merchant_id"`
	MerchantGroupID        string           `json:"merchant_group_id,omitempty"`
	Amount                 int              `json:"amount"`
	Currency               string           `json:"currency,omitempty"`
	OrderNumber            string           `json:"order_number,omitempty"`
	BankTransactionID      string           `json:"bank_transaction_id,omitempty"`
	Details                string           `json:"details"`
	Cardholder             Cardholder       `json:"cardholder,omitempty"`
	CardholderVerify       CardholderVerify `json:"cardholder_verify,omitempty"`
	Instalment             int              `json:"instalment,omitempty"`
	DelayCaptureInDays     int              `json:"delay_capture_in_days,omitempty"`
	ThreeDomainSecure      bool             `json:"three_domain_secure,omitempty"`
	ResultURL              ResultURL        `json:"result_url,omitempty"`
	Remember               bool             `json:"remember,omitempty"`
	Redeem                 bool             `json:"redeem,omitempty"`
	AdditionalData         string           `json:"additional_data,omitempty"`
	LinePayProductImageURL string           `json:"line_pay_product_image_url,omitempty"`
}

// PayByPrimeResp json
type PayByPrimeResp struct {
	Status                int                   `json:"status,omitempty"`
	Msg                   string                `json:"msg,omitempty"`
	RecTradeID            string                `json:"rec_trade_id,omitempty"`
	BankTransactionID     string                `json:"bank_transaction_id,omitempty"`
	AuthCode              string                `json:"auth_code,omitempty"`
	CardSecret            CardSecret            `json:"card_secret,omitempty"`
	Amount                int                   `json:"amount,omitempty"`
	Currency              string                `json:"currency,omitempty"`
	CardInfo              CardInfo              `json:"card_info,omitempty"`
	OrderNumber           string                `json:"order_number,omitempty"`
	Acquirer              string                `json:"acquirer,omitempty"`
	TransactionTimeMillis int                   `json:"transaction_time_millis,omitempty"`
	BankTransactionTime   BankTransactionTime1  `json:"bank_transaction_time,omitempty"`
	BankResultCode        string                `json:"bank_result_code,omitempty"`
	BankResultMsg         string                `json:"bank_result_msg,omitempty"`
	PaymentURL            string                `json:"payment_url,omitempty"`
	InstalmentInfo        InstalmentInfo        `json:"instalment_info,omitempty"`
	RedeemInfo            RedeemInfo            `json:"redeem_info,omitempty"`
	CardIdentifier        string                `json:"card_identifier,omitempty"`
	MerchantReferenceInfo MerchantReferenceInfo `json:"merchant_reference_info,omitempty"`
}

// PayByTokenReq json
type PayByTokenReq struct {
	CardKey            string    `json:"card_key"`
	CardToken          string    `json:"card_token"`
	PartnerKey         string    `json:"partner_key"`
	MerchantID         string    `json:"merchant_id"`
	MerchantGroupID    string    `json:"merchant_group_id,omitempty"`
	Amount             int       `json:"amount"`
	Currency           string    `json:"currency,omitempty"`
	OrderNumber        string    `json:"order_number,omitempty"`
	BankTransactionID  string    `json:"bank_transaction_id,omitempty"`
	Details            string    `json:"details"`
	Instalment         int       `json:"instalment,omitempty"`
	DelayCaptureInDays int       `json:"delay_capture_in_days,omitempty"`
	ThreeDomainSecure  bool      `json:"three_domain_secure,omitempty"`
	ResultURL          ResultURL `json:"result_url,omitempty"`
	FraudID            string    `json:"fraud_id,omitempty"`
	CardCcv            string    `json:"card_ccv,omitempty"`
	Redeem             bool      `json:"redeem,omitempty"`
}

// PayByTokenResp json
type PayByTokenResp struct {
	Status                int                   `json:"status,omitempty"`
	Msg                   string                `json:"msg,omitempty"`
	RecTradeID            string                `json:"rec_trade_id,omitempty"`
	BankTransactionID     string                `json:"bank_transaction_id,omitempty"`
	Amount                int                   `json:"amount,omitempty"`
	Currency              string                `json:"currency"`
	AuthCode              string                `json:"auth_code,omitempty"`
	CardInfo              CardInfo              `json:"card_info"`
	OrderNumber           string                `json:"order_number,omitempty"`
	Acquirer              string                `json:"acquirer,omitempty"`
	TransactionTimeMillis int                   `json:"transaction_time_millis,omitempty"`
	BankTransactionTime   BankTransactionTime1  `json:"bank_transaction_time,omitempty"`
	BankResultCode        string                `json:"bank_result_code,omitempty"`
	BankResultMsg         string                `json:"bank_result_msg,omitempty"`
	PaymentURL            string                `json:"payment_url,omitempty"`
	InstalmentInfo        InstalmentInfo        `json:"instalment_info,omitempty"`
	RedeemInfo            RedeemInfo            `json:"redeem_info,omitempty"`
	CardIdentifier        string                `json:"card_identifier,omitempty"`
	MerchantReferenceInfo MerchantReferenceInfo `json:"merchant_reference_info,omitempty"`
}

// PayInfo json
type PayInfo struct {
	Method                 string `json:"method,omitempty"`
	MaskedCreditCardNumber string `json:"masked_credit_card_number,omitempty"`
	Point                  int    `json:"point,omitempty"`
	Discount               int    `json:"discount,omitempty"`
}

// RecordReq json
type RecordReq struct {
	PartnerKey     string  `json:"partner_key"`
	RecordsPerPage int     `json:"records_per_page"`
	Page           int     `json:"page"`
	Filters        Filter  `json:"filters,omitempty"`
	OrderBy        OrderBy `json:"order_by,omitempty"`
}

// RecordResp json
type RecordResp struct {
	Status               int           `json:"status,omitempty"`
	Msg                  string        `json:"msg,omitempty"`
	RecordsPerPage       int           `json:"records_per_page,omitempty"`
	Page                 int           `json:"page,omitempty"`
	TotalPageCount       int           `json:"total_page_count,omitempty"`
	NumberOfTransactions int           `json:"number_of_transactions,omitempty"`
	TradeRecords         []TradeRecord `json:"trade_records,omitempty"`
}

// RedeemExtraInfo json
type RedeemExtraInfo struct {
	RedeemUsed    string `json:"redeem_used,omitempty"`
	CreditAmt     string `json:"credit_amt,omitempty"`
	RedeemBalance string `json:"redeem_balance,omitempty"`
	RedeemType    string `json:"redeem_type,omitempty"`
}

// RedeemInfo json
type RedeemInfo struct {
	UsedPoint    string          `json:"used_point,omitempty"`
	Balance      string          `json:"balance,omitempty"`
	OffsetAmount string          `json:"offset_amount,omitempty"`
	DueAmount    string          `json:"due_amount,omitempty"`
	ExtraInfo    RedeemExtraInfo `json:"extra_info,omitempty"`
}

// RefundCancelReq json
type RefundCancelReq struct {
	PartnerKey string `json:"partner_key"`
	RecTradeID string `json:"rec_trade_id"`
	RefundID   string `json:"refund_id,omitempty"`
}

// RefundCancelResp json
type RefundCancelResp struct {
	Status     int      `json:"status"`
	Msg        string   `json:"msg"`
	Currency   string   `json:"currency"`
	RecTradeID string   `json:"rec_trade_id"`
	Result     []Result `json:"result"`
}

// RefundReq json
type RefundReq struct {
	PartnerKey     string `json:"partner_key"`
	RecTradeID     string `json:"rec_trade_id"`
	BankRefundID   string `json:"bank_refund_id,omitempty"`
	Amount         int    `json:"amount,omitempty"`
	AdditionalData string `json:"additional_data,omitempty"`
}

// RefundResp json
type RefundResp struct {
	Status         int    `json:"status"`
	Msg            string `json:"msg"`
	RefundID       string `json:"refund_id"`
	RefundAmount   int    `json:"refund_amount"`
	IsCaptured     bool   `json:"is_captured"`
	BankResultCode string `json:"bank_result_code"`
	BankResultMsg  string `json:"bank_result_msg"`
	Currency       string `json:"currency"`
}

// RemoveCardReq json
type RemoveCardReq struct {
	PartnerKey string `json:"partner_key"`
	CardKey    string `json:"card_key"`
	CardToken  string `json:"card_token"`
}

// RemoveCardResp json
type RemoveCardResp struct {
	Status int    `json:"status,omitempty"`
	Msg    string `json:"msg,omitempty"`
}

// Result json
type Result struct {
	RefundID       string `json:"refund_id"`
	Amount         int    `json:"amount"`
	BankResultCode string `json:"bank_result_code"`
	BankResultMsg  string `json:"bank_result_msg"`
}

// ResultURL json
type ResultURL struct {
	FrontendRedirectURL string `json:"frontend_redirect_url"`
	BackendNotifyURL    string `json:"backend_notify_url"`
}

// Time json
type Time struct {
	StartTime int64 `json:"start_time,omitempty"`
	EndTime   int64 `json:"end_time,omitempty"`
}

// TradeHistory json
type TradeHistory struct {
	Amount                int    `json:"amount,omitempty"`
	Action                int    `json:"action,omitempty"`
	Millis                int    `json:"millis,omitempty"`
	Success               bool   `json:"success,omitempty"`
	CanRefundCancelMillis int    `json:"can_refund_cancel_millis,omitempty"`
	RefundID              string `json:"refund_id,omitempty"`
	RequestID             string `json:"request_id,omitempty"`
	BankRefundID          string `json:"bank_refund_id,omitempty"`
	IsPending             bool   `json:"is_pending,omitempty"`
	Status                int    `json:"status,omitempty"`
	Msg                   string `json:"msg,omitempty"`
}

// TradeHistoryReq json
type TradeHistoryReq struct {
	PartnerKey string `json:"partner_key"`
	RecTradeID string `json:"rec_trade_id"`
}

// TradeHistoryResp json
type TradeHistoryResp struct {
	RecTradeID   string         `json:"rec_trade_id,omitempty"`
	Currency     string         `json:"currency,omitempty"`
	TradeHistory []TradeHistory `json:"trade_history,omitempty"`
}

// TradeRecord json
type TradeRecord struct {
	RecTradeID                 string                `json:"rec_trade_id,omitempty"`
	AuthCode                   string                `json:"auth_code,omitempty"`
	MerchantID                 string                `json:"merchant_id,omitempty"`
	MerchantName               string                `json:"merchant_name,omitempty"`
	AppName                    string                `json:"app_name,omitempty"`
	Time                       int                   `json:"time,omitempty"`
	Amount                     int                   `json:"amount,omitempty"`
	RefundedAmount             int                   `json:"refunded_amount,omitempty"`
	RecordStatus               int                   `json:"record_status,omitempty"`
	BankTransactionID          string                `json:"bank_transaction_id,omitempty"`
	CapMillis                  int                   `json:"cap_millis,omitempty"`
	OriginalAmount             int                   `json:"original_amount,omitempty"`
	BankTransactionStartMillis int                   `json:"bank_transaction_start_millis,omitempty"`
	BankTransactionEndMillis   int                   `json:"bank_transaction_end_millis,omitempty"`
	IsCaptured                 bool                  `json:"is_captured,omitempty"`
	BankResultCode             string                `json:"bank_result_code,omitempty"`
	BankResultMsg              string                `json:"bank_result_msg,omitempty"`
	PartialCardNumber          string                `json:"partial_card_number,omitempty"`
	PaymentMethod              string                `json:"payment_method,omitempty"`
	Details                    string                `json:"details,omitempty"`
	Cardholder                 Cardholder            `json:"cardholder,omitempty"`
	Currency                   string                `json:"currency,omitempty"`
	MerchantReferenceInfo      MerchantReferenceInfo `json:"merchant_reference_info,omitempty"`
	ThreeDomainSecure          bool                  `json:"three_domain_secure,omitempty"`
	PayByInstalment            bool                  `json:"pay_by_instalment,omitempty"`
	InstalmentInfo             InstalmentInfo        `json:"instalment_info,omitempty"`
	OrderNumber                string                `json:"order_number,omitempty"`
	PayInfo                    PayInfo               `json:"pay_info,omitempty"`
	PayByRedeem                bool                  `json:"pay_by_redeem,omitempty"`
	RedeemInfo                 RedeemInfo            `json:"redeem_info,omitempty"`
	CardIdentifier             string                `json:"card_identifier,omitempty"`
	CardInfo                   CardInfo              `json:"card_info"`
}

// BackendNotifyReq for TapPay backend notify url
type BackendNotifyReq struct {
	RecTradeID            string                `json:"rec_trade_id"`            // TapPay 伺服器產生的交易識別字串，將於查詢交易、退款時使用
	AuthCode              string                `json:"auth_code"`               // 銀行授權碼
	BankTransactionID     string                `json:"bank_transaction_id"`     // 銀行端訂單編號
	OrderNumber           string                `json:"order_number"`            // 自定義的訂單編號，用於 TapPay 做訂單識別
	Amount                int                   `json:"amount"`                  // 金額
	Status                int                   `json:"status"`                  // 交易代碼
	Msg                   string                `json:"msg"`                     // 交易訊息
	TransactionTimeMillis int64                 `json:"transaction_time_millis"` // 交易時間
	PayInfo               PayInfo               `json:"pay_info"`                // 交易使用 LINE Pay 時，由 LINE Pay 回傳訊息
	Acquirer              string                `json:"acquirer"`                // 收單銀行 / 金流處理器
	CardIdentifier        string                `json:"card_identifier"`         // 信用卡識別碼。每張信用卡只會對到一組識別碼
	BankResultCode        string                `json:"bank_result_code"`        // 銀行回傳的程式碼
	BankResultMsg         string                `json:"bank_result_msg"`         // 銀行回傳的訊息
	MerchantReferenceInfo MerchantReferenceInfo `json:"merchant_reference_info"` // TapPay 後台使用 Co-brand card management 功能，且交易卡號符合設定時，將會回傳此參數
	InstalmentInfo        InstalmentInfo        `json:"instalment_info"`         // 使用分期付款時回傳
	RedeemInfo            RedeemInfo            `json:"redeem_info"`             // 使用紅利時回傳
}
