package tappay

import (
	"bytes"
	"encoding/json"
	"io"
	"net"
	"net/http"
	"time"
)

const productURL = "https://prod.tappaysdk.com"
const sandboxURL = "https://sandbox.tappaysdk.com"

const bindCardURL = "/tpc/card/bind"
const capCancelURL = "/tpc/transaction/cap/cancel"
const capTodayURL = "/tpc/transaction/cap"
const cardMetadataURL = "/tpc/card/metadata"
const getBarcodeURL = "/tpc/direct-pay/get-barcode"
const getMemberCardURL = "/tpc/direct-pay/get-member-card"
const payByBarcodeURL = "/tpc/payment/pay-by-barcode"
const payByPrimeURL = "/tpc/payment/pay-by-prime"
const payByTokenURL = "/tpc/payment/pay-by-token"
const recordURL = "/tpc/transaction/query"
const refundURL = "/tpc/transaction/refund"
const refundCancelURL = "/tpc/transaction/refund/cancel"
const removeCardURL = "/tpc/card/remove"
const tradeHistoryURL = "/tpc/transaction/trade-history"

// A Client is an TapPay client
type Client struct {
	IsProduction bool
	PartnerKey   string
}

// NewClient create a new TapPay client
func NewClient(partnerKey string, isProduction bool) Client {
	return Client{
		IsProduction: isProduction,
		PartnerKey:   partnerKey,
	}
}

func getURL(isProduction bool) string {
	if isProduction {
		return productURL
	}

	return sandboxURL
}

func post(partnerKey string, URL string, data interface{}, setDialTime, setTime int) ([]byte, error) {
	json, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}
	req, err := http.NewRequest("POST", URL, bytes.NewBuffer(json))
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("x-api-key", partnerKey)

	var client *http.Client
	switch {
	case setTime < 0:
		// 0秒timeout for 測試
		client = &http.Client{
			Timeout: time.Nanosecond,
		}
	case setTime == 0:
		// 沒有timeout
		client = &http.Client{}
	case setDialTime >= setTime:
		client = &http.Client{
			Timeout: time.Duration(setTime) * time.Second,
		}
	default:
		// 總等待[setTime]秒 如果[setDialTime]秒內TCP Connection都還未建立則提早回應錯誤
		// DNS解Domain -> **建立TCP Connection -> 傳送HTTP Request -> 等待伺服器回應 -> 接收HTTP Response
		transport := &http.Transport{
			DialContext: (&net.Dialer{
				Timeout: time.Duration(setDialTime) * time.Second,
			}).DialContext,
		}
		client = &http.Client{
			Transport: transport,
			Timeout:   time.Duration(setTime) * time.Second,
		}
	}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	return io.ReadAll(resp.Body)
}

// BindCard API
func (c *Client) BindCard(data BindCardReq, setDialTime, setTime int) (BindCardResp, error) {
	URL := getURL(c.IsProduction) + bindCardURL

	var response BindCardResp
	body, err := post(c.PartnerKey, URL, data, setDialTime, setTime)

	if err == nil {
		err = json.Unmarshal(body, &response)
	}

	return response, err
}

// CapCancel API
func (c *Client) CapCancel(data CapCancelReq, setDialTime, setTime int) (CapCancelResp, error) {
	URL := getURL(c.IsProduction) + capCancelURL

	var response CapCancelResp
	body, err := post(c.PartnerKey, URL, data, setDialTime, setTime)

	if err == nil {
		err = json.Unmarshal(body, &response)
	}

	return response, err
}

// CapToday API
func (c *Client) CapToday(data CapTodayReq, setDialTime, setTime int) (CapTodayResp, error) {
	URL := getURL(c.IsProduction) + capTodayURL

	var response CapTodayResp
	body, err := post(c.PartnerKey, URL, data, setDialTime, setTime)

	if err == nil {
		err = json.Unmarshal(body, &response)
	}

	return response, err
}

// CardMetadata API
func (c *Client) CardMetadata(data CardMetadataReq, setDialTime, setTime int) (CardMetadataResp, error) {
	URL := getURL(c.IsProduction) + cardMetadataURL

	var response CardMetadataResp
	body, err := post(c.PartnerKey, URL, data, setDialTime, setTime)

	if err == nil {
		err = json.Unmarshal(body, &response)
	}

	return response, err
}

// GetBarcode API
func (c *Client) GetBarcode(data GetBarcodeReq, setDialTime, setTime int) (GetBarcodeResp, error) {
	URL := getURL(c.IsProduction) + getBarcodeURL

	var response GetBarcodeResp
	body, err := post(c.PartnerKey, URL, data, setDialTime, setTime)

	if err == nil {
		err = json.Unmarshal(body, &response)
	}

	return response, err
}

// GetMemberCard API
func (c *Client) GetMemberCard(data GetMemberCardReq, setDialTime, setTime int) (GetMemberCardResp, error) {
	URL := getURL(c.IsProduction) + getMemberCardURL

	var response GetMemberCardResp
	body, err := post(c.PartnerKey, URL, data, setDialTime, setTime)

	if err == nil {
		err = json.Unmarshal(body, &response)
	}

	return response, err
}

// PayByBarcode API
func (c *Client) PayByBarcode(data PayByBarcodeReq, setDialTime, setTime int) (PayByBarcodeResp, error) {
	URL := getURL(c.IsProduction) + payByBarcodeURL

	var response PayByBarcodeResp
	body, err := post(c.PartnerKey, URL, data, setDialTime, setTime)

	if err == nil {
		err = json.Unmarshal(body, &response)
	}

	return response, err
}

// PayByPrime API
func (c *Client) PayByPrime(data PayByPrimeReq, setDialTime, setTime int) (PayByPrimeResp, error) {
	URL := getURL(c.IsProduction) + payByPrimeURL

	var response PayByPrimeResp
	body, err := post(c.PartnerKey, URL, data, setDialTime, setTime)

	if err == nil {
		err = json.Unmarshal(body, &response)
	}

	return response, err
}

// PayByToken API
func (c *Client) PayByToken(data PayByTokenReq, setDialTime, setTime int) (PayByTokenResp, error) {
	URL := getURL(c.IsProduction) + payByTokenURL

	var response PayByTokenResp
	body, err := post(c.PartnerKey, URL, data, setDialTime, setTime)

	if err == nil {
		err = json.Unmarshal(body, &response)
	}

	return response, err
}

// Record API
func (c *Client) Record(data RecordReq, setDialTime, setTime int) (RecordResp, error) {
	URL := getURL(c.IsProduction) + recordURL

	var response RecordResp
	body, err := post(c.PartnerKey, URL, data, setDialTime, setTime)

	if err == nil {
		err = json.Unmarshal(body, &response)
	}

	return response, err
}

// Refund API
func (c *Client) Refund(data RefundReq, setDialTime, setTime int) (RefundResp, error) {
	URL := getURL(c.IsProduction) + refundURL

	var response RefundResp
	body, err := post(c.PartnerKey, URL, data, setDialTime, setTime)

	if err == nil {
		err = json.Unmarshal(body, &response)
	}

	return response, err
}

// RefundCancel API
func (c *Client) RefundCancel(data RefundCancelReq, setDialTime, setTime int) (RefundCancelResp, error) {
	URL := getURL(c.IsProduction) + refundCancelURL

	var response RefundCancelResp
	body, err := post(c.PartnerKey, URL, data, setDialTime, setTime)

	if err == nil {
		err = json.Unmarshal(body, &response)
	}

	return response, err
}

// RemoveCard API
func (c *Client) RemoveCard(data RemoveCardReq, setDialTime, setTime int) (RemoveCardResp, error) {
	URL := getURL(c.IsProduction) + removeCardURL

	var response RemoveCardResp
	body, err := post(c.PartnerKey, URL, data, setDialTime, setTime)

	if err == nil {
		err = json.Unmarshal(body, &response)
	}

	return response, err
}

// TradeHistory API
func (c *Client) TradeHistory(data TradeHistoryReq, setDialTime, setTime int) (TradeHistoryResp, error) {
	URL := getURL(c.IsProduction) + tradeHistoryURL

	var response TradeHistoryResp
	body, err := post(c.PartnerKey, URL, data, setDialTime, setTime)

	if err == nil {
		err = json.Unmarshal(body, &response)
	}

	return response, err
}
